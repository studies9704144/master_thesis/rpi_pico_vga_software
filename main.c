/*
 Description 	    Notation 	Time 	            Width/Freq
Pixel Clock 	    tclk 	    39.7 ns (± 0.5%) 	25.175MHz
Hor Sync Time 	    ths 	    3.813 μs 	        96 Pixels
Hor Back Porch 	    thbp 	    1.907 μs 	        48 Pixels
Hor Front Porch 	thfp 	    0.636 μs 	        16 Pixels
Hor Addr Video Time thaddr 	    25.422 μs 	        640 Pixels
Hor L/R Border 	    thbd 	    0 μs 	            0 Pixels
V Sync Time 	    tvs 	    0.064 ms 	        2 Lines
V Back Porch 	    tvbp 	    1.048 ms 	        33 Lines
V Front Porch 	    tvfp 	    0.318 ms 	        10 Lines
V Addr Video Time 	tvaddr 	    15.253 ms 	        480 Lines
V T/B Border 	    tvbd 	    0 ms 	            0 Lines


*/

#include <stdio.h>
#include "string.h"

#include "hardware/gpio.h"
#include "hardware/adc.h"
#include "pico/stdlib.h"
#include "hardware/timer.h"
#include "hardware/clocks.h"
#include "hardware/structs/clocks.h"
#include "pico/multicore.h"

#define NANO_SECOND_COEFFICIENT 8
#define TIMER_INDEX 0

#define V_SYNC_PIN 10
#define H_SYNC_PIN 11
#define R_COLOR_PIN 12
#define G_COLOR_PIN 13
#define B_COLOR_PIN 14

inline uint32_t ns_to_cycles(uint32_t ns)
{
    return (ns / NANO_SECOND_COEFFICIENT);
}

int main()
{
    uint32_t mask_rgb = 0x00000000;
    mask_rgb = (1 << R_COLOR_PIN) | (1 << G_COLOR_PIN) | (1 << B_COLOR_PIN);

    stdio_init_all();
    printf("Frequency of the core is %ld Hz\n", clock_get_hz(clk_sys));

    gpio_init(V_SYNC_PIN);
    gpio_init(H_SYNC_PIN);
    gpio_init(R_COLOR_PIN);
    gpio_init(G_COLOR_PIN);
    gpio_init(B_COLOR_PIN);

    gpio_set_dir(V_SYNC_PIN, GPIO_OUT);
    gpio_set_dir(H_SYNC_PIN, GPIO_OUT);
    gpio_set_dir(R_COLOR_PIN, GPIO_OUT);
    gpio_set_dir(G_COLOR_PIN, GPIO_OUT);
    gpio_set_dir(B_COLOR_PIN, GPIO_OUT);

    // uint timer = timer_hw_get_index(TIMER_INDEX);
    //  timer_config config = timer_get_default_config(TIMER_INDEX);
    //  timer_init(timer, &config);

    while (true)
    {
        gpio_put(V_SYNC_PIN, false);
        for (int i = 0; i < 2; i++)
        {
            gpio_put(H_SYNC_PIN, false);
            busy_wait_at_least_cycles(ns_to_cycles(3810));
            gpio_put(H_SYNC_PIN, true);
            busy_wait_at_least_cycles(ns_to_cycles(27964));
        }

        gpio_put(V_SYNC_PIN, true);
        for (int i = 0; i < 33; i++)
        {
            gpio_put(H_SYNC_PIN, false);
            busy_wait_at_least_cycles(ns_to_cycles(3810));
            gpio_put(H_SYNC_PIN, true);
            busy_wait_at_least_cycles(ns_to_cycles(27964));
        }

        for (int j = 0; j < 480; j++)
        {
            gpio_clr_mask(mask_rgb | (1 << H_SYNC_PIN));
            busy_wait_at_least_cycles(ns_to_cycles(3810));
            gpio_set_mask(mask_rgb | (1 << H_SYNC_PIN));
            busy_wait_at_least_cycles(ns_to_cycles(27964));
        }

        gpio_clr_mask(mask_rgb);

        for (int i = 0; i < 10; i++)
        {
            gpio_put(H_SYNC_PIN, false);
            busy_wait_at_least_cycles(ns_to_cycles(3810));
            gpio_put(H_SYNC_PIN, true);
            busy_wait_at_least_cycles(ns_to_cycles(27964));
        }
    }
}

/*

    gpio_set_mask(mask);
    gpio_clr_mask(mask);
    const uint LED_PIN = PICO_DEFAULT_LED_PIN;

    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    gpio_put(LED_PIN, true);
    sleep_ms(250);
    gpio_put(LED_PIN, false);
    sleep_ms(250);
    gpio_put(LED_PIN, true);
*/
